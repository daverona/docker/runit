FROM ubuntu:22.04

RUN apt-get update \
  && apt-get install --yes --quiet --no-install-recommends \
    runit \
    tzdata \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

#RUN apt-get update \
#  && apt-get install --yes --quiet --no-install-recommends \
#    munge \
#  && apt-get clean && rm -rf /var/lib/apt/lists/* \
#  && mkdir -p /var/run/munge && chown munge:munge /var/run/munge \
#  && mkdir -p /etc/service/munge \
#  && {\
#    echo "#!/bin/bash"; \
#    echo "set -e"; \
#    echo "chown munge:munge /var/{lib,log,run}/munge"; \
#    echo "chown -R munge:munge /etc/munge"; \
#    echo "chmod 700 /etc/munge"; \
#    echo "chmod 400 /etc/munge/munge.key"; \
#    echo "exec chpst -u munge /usr/sbin/munged -F"; \
#  } > /etc/service/munge/run \
#  && chmod +x /etc/service/munge/run

CMD ["runsvdir", "/etc/service"]
